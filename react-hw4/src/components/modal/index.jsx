import "./modal.scss";

export function Modal(props) {
  function handleOverlayClick(event) {
    if (event.target === event.currentTarget) {
      props.onClose();
    }
  }
  const buttonOnClick = props.removeCard
    ? (event) => {
        props.removeCard(props.id);
        handleOverlayClick(event);
      }
    : (event) => {
        props.addToCart(props.id);
        handleOverlayClick(event);
      };
  return (
    <>
      <div className="modal-overlay" onClick={handleOverlayClick}></div>
      <div className="modal">
        <div className="modal-closedBtn" onClick={handleOverlayClick}></div>
        <h2 className="modal-title">{props.removeCard ? "Видалення товару" : "Підтвердження покупки"}</h2>
        <div className="modal-item">
          <img src={props.src} alt="paint" />
          <p>{props.text}</p>
        </div>
        <p className="modal-descr modal-descr--price">{props.price} ₴</p>
        <p className="modal-descr">
          {" "}
          {props.removeCard ? "Бажаєте видалити товар з кошику ?" : "Бажаєте додати товар в кошик ?"}
        </p>
        <div className="modal-buttons">
          <button onClick={buttonOnClick}>{props.removeCard ? "Видалити" : "Додати в кошик"}</button>
          <button onClick={handleOverlayClick}>
            {props.removeCard ? "Повернутися в кошик" : "Продовжити покупки"}
          </button>
        </div>
      </div>
    </>
  );
}

export default Modal;
