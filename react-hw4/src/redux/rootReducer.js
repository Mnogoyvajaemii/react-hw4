// Core
import { combineReducers } from "redux";

// Reducers
import { dataReducer as items } from "./reducers/saveDataToStore ";
import { showModalReducer as modal } from "./reducers/handleModal";

export const rootReducer = combineReducers({ items, modal });
