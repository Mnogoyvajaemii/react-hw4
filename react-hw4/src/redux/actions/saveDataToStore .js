import { storeData } from "../types";

export const saveDataToStore = (data) => ({
  type: storeData.SAVE_DATA_TO_STORE,
  payload: data,
});
