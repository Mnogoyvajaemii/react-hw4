import { handleModal } from "../types";

export const handleShow = (boolean) => ({
  type: handleModal.SHOW_MODAL,
  payload: boolean,
});
