import { handleModal } from "../types";

const initialState = {
  showModal: false,
};

export const showModalReducer = (state = initialState, action) => {
  switch (action.type) {
    case handleModal.SHOW_MODAL:
      return { ...state, showModal: action.payload };
    default:
      return state;
  }
};
