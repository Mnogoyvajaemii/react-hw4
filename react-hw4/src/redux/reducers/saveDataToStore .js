import { storeData } from "../types";

const initialState = {
  dataArray: [],
};

export const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case storeData.SAVE_DATA_TO_STORE:
      return { ...state, dataArray: action.payload };
    default:
      return state;
  }
};
