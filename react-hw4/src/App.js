import "./App.css";
import { Routes, Route } from "react-router-dom";
import { Home, Favorite, Cart, NotFound } from "./pages";
import { getProductsFromLS } from "./functions/getProductsFromLS";
import { useState, useEffect } from "react";
import Header from "./components/header";
import { useDispatch, useSelector } from "react-redux";
import { saveDataToStore } from "./redux/actions/saveDataToStore ";

function App() {
  const [favItems, setFavItems] = useState(getProductsFromLS("favItems"));
  const [cartItems, setCartItems] = useState(getProductsFromLS("cartItems"));
  // const [cardData, setCardData] = useState(null);
  const [dataLoaded, setDataLoaded] = useState(false);
  const dispatch = useDispatch();

  const cardData = useSelector((state) => state.items.dataArray);

  useEffect(() => {
    fetch("./items.json")
      .then((res) => res.json())
      .then((data) => {
        dispatch(saveDataToStore(data));
        setDataLoaded(true);
      })
      .catch((error) => console.log(error));
  }, []);

  const addToFav = (id) => {
    const arr = [...favItems];
    if (!arr.includes(id)) {
      arr.push(id);
    } else {
      const index = arr.findIndex((item) => item === id);
      arr.splice(index, 1);
    }
    localStorage.setItem("favItems", JSON.stringify(arr));
    setFavItems(arr);
  };

  const addToCart = (id) => {
    const arr = [...cartItems];
    if (!arr.includes(id)) {
      arr.push(id);
    }
    localStorage.setItem("cartItems", JSON.stringify(arr));
    setCartItems(arr);
  };
  return (
    <>
      <Header favItems={favItems} cartItems={cartItems} cardData={cardData} dataLoaded={dataLoaded} />

      <Routes>
        <Route
          path="/"
          element={
            <Home
              addToCart={addToCart}
              addToFav={addToFav}
              favItems={favItems}
              cartItems={cartItems}
              setFav={setFavItems}
              setCart={setCartItems}
              cardData={cardData}
              dataLoaded={dataLoaded}
            />
          }
        />
        <Route
          path="/favorite"
          element={
            <Favorite
              addToCart={addToCart}
              addToFav={addToFav}
              favItems={favItems}
              cartItems={cartItems}
              setFav={setFavItems}
              setCart={setCartItems}
              cardData={cardData}
              dataLoaded={dataLoaded}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              addToCart={addToCart}
              addToFav={addToFav}
              favItems={favItems}
              cartItems={cartItems}
              setFav={setFavItems}
              setCart={setCartItems}
            />
          }
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
