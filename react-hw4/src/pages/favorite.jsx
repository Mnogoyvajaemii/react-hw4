import Main from "../components/main";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

export function Favorite({ favItems, cartItems, setFav, addToCart, addToFav }) {
  const [cardData, setCardData] = useState(null);
  const [dataLoaded, setDataLoaded] = useState(false);

  const itemsArray = useSelector((state) => state.items.dataArray);

  useEffect(() => {
    const newData = favItems.map((favItem) => itemsArray.find((cardItem) => cardItem["item number"] === favItem));
    setCardData(newData);
    setDataLoaded(true);
  }, [favItems]);

  return (
    <>
      <div className="container">
        <h1 style={{ marginBottom: 0, fontWeight: 500, color: "rgb(0,60,128)" }}>Обране</h1>
      </div>
      <Main
        setFav={setFav}
        addToCart={addToCart}
        addToFav={addToFav}
        favItems={favItems}
        cartItems={cartItems}
        cardData={cardData}
        dataLoaded={dataLoaded}
      />
    </>
  );
}
