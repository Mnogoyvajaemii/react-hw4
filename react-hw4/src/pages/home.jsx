import Main from "../components/main";

export function Home(props) {
  return (
    <>
      <Main
        setFav={props.setFav}
        addToCart={props.addToCart}
        addToFav={props.addToFav}
        favItems={props.favItems}
        cartItems={props.cartItems}
        cardData={props.cardData}
        dataLoaded={props.dataLoaded}
      />
    </>
  );
}
