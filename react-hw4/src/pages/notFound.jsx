import { Link } from "react-router-dom";

export function NotFound() {
  return (
    <>
      <h1>Page not found</h1>
      <p>
        Return to <Link to="/">Home page</Link>
      </p>
    </>
  );
}
