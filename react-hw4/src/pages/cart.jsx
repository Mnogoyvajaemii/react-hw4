import Main from "../components/main";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

export function Cart({ favItems, cartItems, setFav, setCart, addToFav, addToCart }) {
  const [cardData, setCardData] = useState(null);
  const [dataLoaded, setDataLoaded] = useState(false);

  const itemsArray = useSelector((state) => state.items.dataArray);

  useEffect(() => {
    const newData = cartItems.map((cartItem) => itemsArray.find((cardItem) => cardItem["item number"] === cartItem));
    setCardData(newData);
    setDataLoaded(true);
  }, [cartItems]);

  const removeFromCart = (id) => {
    const arr = [...cartItems];
    if (arr.includes(id)) {
      const index = arr.findIndex((item) => item === id);
      arr.splice(index, 1);
    }
    localStorage.setItem("cartItems", JSON.stringify(arr));
    setCart(arr);
  };

  return (
    <>
      <div className="container">
        <h1 style={{ marginBottom: 0, fontWeight: 500, color: "rgb(0,60,128)" }}>Кошик</h1>
      </div>
      <Main
        removeCard={removeFromCart}
        setFav={setFav}
        addToCart={addToCart}
        addToFav={addToFav}
        favItems={favItems}
        cartItems={cartItems}
        cardData={cardData}
        dataLoaded={dataLoaded}
      />
    </>
  );
}
